import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import AchievementsScreen from "../screens/Achievements.js"
import AchievementScreen from "../screens/Achievement.js"

const Stack = createStackNavigator()

const AchievementsNavigation = ({ navigation }) => {
    return (
        <Stack.Navigator 
            initialRouteName="Achievements"
            headerMode="none"
        >
            <Stack.Screen 
                name="Achievements"
                component={AchievementsScreen}
            />
            <Stack.Screen 
                name="Achievement"
                component={AchievementScreen}
            />
        </Stack.Navigator>
    )
}

export default AchievementsNavigation