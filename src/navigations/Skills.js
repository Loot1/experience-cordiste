import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import SkillScreen from "../screens/Skill.js"
import SkillsScreen from "../screens/Skills.js"
import AchievementScreen from "../screens/Achievement.js"

const Stack = createStackNavigator()

const SkillsNavigation = ({ navigation }) => {
    return (
        <Stack.Navigator
            initialRouteName="Skills"
            headerMode="none"
        >
            <Stack.Screen 
                name="Skills"
                component={SkillsScreen}
            />
            <Stack.Screen 
                name="Skill"
                component={SkillScreen}
            />
            <Stack.Screen 
                name="Achievement"
                component={AchievementScreen}
            />
        </Stack.Navigator>
    )
}

export default SkillsNavigation