import React from 'react'
import { Platform, View, Image, Text, TouchableOpacity } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { useFonts } from 'expo-font'

const Title = ({title, returnArrow = false, navigation = null}) => {
    const [loaded] = useFonts({
        Chiller: require('../../assets/fonts/Chiller.ttf')
    })
    if (!loaded) {
        return null
    }
    return (
        <View style={{ flexDirection:"row", marginLeft: 10 }}>
            <Image
                style={{ flex: 1, width: 50, height: 79 }}
                source={require('../../assets/img/logo.png')}
            />
            {returnArrow ?
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={{ flex: 1, alignItems: "center", justifyContent: "center", paddingLeft: 10 }}
                >
                    <Ionicons
                        name={`${Platform.OS === "ios" ? "ios" : "md"}-arrow-back`}
                        color="white"
                        size={30}
                    />
                </TouchableOpacity>
            : null}
            <Text style={{ flex: 6, fontSize: 40, fontFamily:"Chiller", color:"white", alignSelf:"center", paddingLeft: 10 }}>
                {title.toUpperCase()}
            </Text>
        </View>

    )
}

export default Title