import React from 'react'
import { Text, SafeAreaView, ScrollView, View, Image, ImageBackground, Dimensions } from 'react-native'
import MapView, { Marker } from 'react-native-maps'
import { Video } from 'expo-av'
import { useFonts } from 'expo-font'

import Title from "../components/Title.js"

const window = Dimensions.get("window")

const HomeScreen = () => {
    const video = React.useRef(null)
    const [loaded] = useFonts({
        Gothic: require('../../assets/fonts/Gothic.ttf')
    })
    if (!loaded) {
        return null
    }
    return (
        <ImageBackground style={{width: window.width, height:window.height, resizeMode:"cover" }} source={require('../../assets/img/fond.gif')}>
            <SafeAreaView style={{ flex: 1 }}>
                <ScrollView>
                    <Title title="Expérience Cordiste"/>
                    <View style={{ margin: 20, alignItems:"center" }}>
                        <Text style={{ color:"white", fontFamily:"Gothic", fontSize: 20, textAlign:"justify" }}>
                            Implantés à Amiens depuis 2008, nous intervenons dans les Hauts-de-France, en Normandie, en région parisienne et pouvons nous déplacer sur tout le territoire français.
                            Nous garantissons à nos clients un travail soigné, de qualité et en toute sécurité.
                        </Text>
                        <Image
                            style={{ flex: 1, width: 333, height: 188, borderWidth: 3, borderColor:"white", marginVertical: 25 }}
                            source={require('../../assets/img/cover.jpg')}
                        />
                        <Text style={{ color:"white", fontFamily:"Gothic", fontSize: 20, textAlign:"justify" }}>
                            Expérience Cordiste est une entreprise spécialisée dans les travaux d'accès difficiles, les travaux en hauteur et  les travaux sur cordes ou dits travaux acrobatiques.
                        </Text>
                        <Video
                            ref={video}
                            style={{flex: 1, alignSelf: 'center', width: 320, height: 200, borderWidth: 3, borderColor:"white", marginVertical: 25 }}
                            source={require('../../assets/img/video.mp4')}
                            resizeMode="cover"
                            useNativeControls
                        />
                        <Text style={{ color:"white", fontFamily:"Gothic", fontSize: 20, textAlign:"justify" }}>
                            Partenaire d'industriels, d'entreprises générales du bâtiments, d'entreprises de nettoyage, de Syndicats de co-propriété, de coopératives agricoles, de collectivités locales et d'agences de communication notre équipe de professionnels est au service des gestionnaires de patrimoine pour apporter une solution réactive, économique et rigoureuse.
                        </Text>
                        <MapView
                            style={{height: 350, width: 350, borderWidth: 3, borderColor:"white", borderRadius: 30, marginVertical: 25 }}
                            region={{
                                latitude: 49.88619911168173,
                                longitude: 2.2879524943570777,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421
                            }}
                            scrollEnabled={false}
                            zoomEnabled={false}
                            minZoomLevel={13}
                        >
                            <Marker
                                title="Expérience Cordiste"
                                coordinate={{ latitude : 49.88619911168173 , longitude : 2.2879524943570777 }}
                                pinColor="red"
                            />
                        </MapView>
                        <Text style={{ color:"white", fontFamily:"Gothic", fontSize: 20, textAlign:"justify", marginBottom: 80 }}>
                            Pour l'ensemble de nos activités, notre personnel est qualifié et expérimenté aux techniques d'accès difficiles et possède les compétences métiers nécessaires à la bonne réalisation de nos interventions.
                        </Text>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </ImageBackground>
    )
}

export default HomeScreen