import React from 'react'
import { Text, SafeAreaView, View, Image } from 'react-native'
import { useFonts } from 'expo-font'

import Title from "../components/Title.js"

const AchievementScreen = ({ navigation, route }) => {
    const [loaded] = useFonts({
        Gothic: require('../../assets/fonts/Gothic.ttf')
    })
    if (!loaded) {
        return null
    }
    const achievement = route.params.paramKey
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor:"#FFD45A" }}>
            <Title title="Réalisations" returnArrow={true} navigation={navigation}/>
            <View style={{ flex: 1, backgroundColor:"#F9920A", borderRadius: 30, marginHorizontal:30, marginVertical:80, alignItems:"center", justifyContent:"center"}}>
                <Text style={{ flex: 1, fontSize: 15, fontFamily:"Gothic", color:"white", textAlign: "center", marginTop: 15 }}>
                    {achievement.title}
                </Text>
                <Image
                    source={{ uri: achievement.img }}
                    style={{ width: 290, height: 400, borderColor:"white", borderWidth: 2, flex: 10 }}
                />
                <Text style={{ flex: 2, fontSize: 15, fontFamily:"Gothic", color:"white", textAlign: "center", marginHorizontal: 15, marginVertical:10 }}>
                    {achievement.desc}
                </Text>
            </View>
        </SafeAreaView>
    )
}

export default AchievementScreen