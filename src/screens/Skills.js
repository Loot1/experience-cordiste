import React from 'react'
import { Text, SafeAreaView, View, Image, FlatList, TouchableOpacity } from 'react-native'
import { useFonts } from 'expo-font'

import Title from "../components/Title.js"

const skills = require("../../skills.json")

const SkillsScreen = ({ navigation }) => {
    const [loaded] = useFonts({
        Chiller: require('../../assets/fonts/Chiller.ttf')
    })
    if (!loaded) {
        return null
    }
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor:"#FFD45A" }}>
            <Title title="Savoir-faire"/>
            <FlatList
                data={skills}
                renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => navigation.navigate("Skill", {paramKey: item})}>
                        <View style={{ flex: 1, padding: 15, alignItems:"center" }}>
                            <View style={{ flex: 6, flexDirection: 'row', alignItems:"center", paddingBottom: 10 }}>
                                <View style={{ height: 1, width: 60, borderWidth: 0.7, borderColor:"white" }}/>
                                <Text style={{ fontSize: 36, fontFamily:"Chiller" , color:"white", textAlign: "center", marginLeft: 20, marginRight: 20 }}>
                                    {item.title.toUpperCase()}
                                </Text>
                                <View style={{ height: 1, width: 60, borderWidth: 0.7, borderColor:"white" }}/>
                            </View>
                            <Image
                                source={{ uri: item.img }}
                                style={{ width: 194, height: 250, flex: 3, borderWidth: 2, borderColor:"white" }}
                            />
                        </View>
                    </TouchableOpacity>
                )}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    )
}

export default SkillsScreen