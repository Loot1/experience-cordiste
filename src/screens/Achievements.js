import React from 'react'
import { Text, SafeAreaView, View, Image, FlatList, TouchableOpacity } from 'react-native'
import { useFonts } from 'expo-font'

import Title from "../components/Title.js"

const achievements = require("../../achievements.json")

const AchievementsScreen = ({ navigation }) => {
    const [loaded] = useFonts({
        Gothic: require('../../assets/fonts/Gothic.ttf')
    })
    if (!loaded) {
        return null
    }
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor:"#FFD45A" }}>
            <Title title="Réalisations"/>
            <FlatList
                data={achievements}
                renderItem={({ item }) => (
                    <View style={{ flex: 1, backgroundColor:"#F9920A", borderRadius: 30, marginHorizontal:30, marginBottom:20, height:230}}>
                        <Text style={{ flex: 1, fontSize: 15, fontFamily:"Gothic", color:"white", textAlign: "center", marginTop: 15 }}>
                            {item.title}
                        </Text>
                        <TouchableOpacity
                            style={{ flex: 10, alignItems:"center", justifyContent:"center" }}
                            onPress={() => navigation.navigate("Achievement", {paramKey: item})}
                        >
                            <Image
                                source={{ uri: item.img }}
                                style={{ width: 290, height: 150, borderColor:"white", borderWidth:2 }}
                            />
                        </TouchableOpacity>
                    </View>
                )}
                keyExtractor={item => item.id}
            />
    </SafeAreaView>
    )
}

export default AchievementsScreen