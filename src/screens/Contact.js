import React from 'react'
import { Alert, SafeAreaView, TextInput, TouchableOpacity, KeyboardAvoidingView, Keyboard, Vibration, Dimensions } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { useFonts } from 'expo-font'

import Title from "../components/Title.js"

const window = Dimensions.get("window")

const ContactScreen = () => {
    const [name, onChangeName] = React.useState("")
    const [colorName, onChangeColorName] = React.useState("white")
    const [mail, onChangeMail] = React.useState("")
    const [colorMail, onChangeColorMail] = React.useState("white")
    const [phone, onChangePhone] = React.useState("")
    const [message, onChangeMessage] = React.useState("")
    const [colorMessage, onChangeColorMessage] = React.useState("white")
    const [loaded] = useFonts({
        Chiller: require('../../assets/fonts/Chiller.ttf')
    })
    if (!loaded) {
        return null
    }
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor:"#FFD45A" }}>
            <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={-100}>
                <Title title="Contactez-nous"/>
                <TextInput
                    style={{
                        borderWidth: 3,
                        borderColor:colorName,
                        borderRadius: 30,
                        padding: window.height/45,
                        marginHorizontal: 40,
                        marginVertical: 15,
                        height: window.height/13,
                        color: "white",
                        fontFamily:"Gothic",
                        fontSize: 18
                    }}
                    onChangeText={onChangeName}
                    value={name}
                    placeholder="Nom*"
                    placeholderTextColor="white"
                    autoCompleteType="name"
                    returnKeyType="done"
                    textContentType="name"
                    onEndEditing={()=> {
                        if(name.length === 0 && colorName === "white") onChangeColorName("#F9920A")
                        else if(colorName === "#F9920A") onChangeColorName("white")
                    }}
                />
                <TextInput
                    style={{
                        borderWidth: 3,
                        borderColor:colorMail,
                        borderRadius: 30,
                        padding: window.height/45,
                        marginHorizontal: 40,
                        marginVertical: 15,
                        height: window.height/13,
                        color: "white",
                        fontFamily:"Gothic",
                        fontSize: 18
                    }}
                    onChangeText={onChangeMail}
                    value={mail}
                    placeholder="Adresse mail*"
                    placeholderTextColor="white"
                    keyboardType="email-address"
                    autoCompleteType="email"
                    returnKeyType="done"
                    textContentType="emailAddress"
                    onEndEditing={()=> {
                        if((mail.length === 0 || !mail.match(/^[a-z0-9\_.-]+@[a-z0-9]+\.[a-z]{2,6}$/)) && colorMail === "white") onChangeColorMail("#F9920A")
                        else if(colorMail === "#F9920A") onChangeColorMail("white")
                    }}
                />
                <TextInput
                    style={{
                        borderWidth: 3,
                        borderColor:"white",
                        borderRadius: 30,
                        padding: window.height/45,
                        marginHorizontal: 40,
                        marginVertical: 15,
                        height: window.height/13,
                        color: "white",
                        fontFamily:"Gothic",
                        fontSize: 18
                    }}
                    onChangeText={onChangePhone}
                    value={phone}
                    placeholder="Téléphone"
                    placeholderTextColor="white"
                    keyboardType="phone-pad"
                    autoCompleteType="tel"
                    returnKeyType="done"
                    textContentType="telephoneNumber"
                />
                <TextInput
                    style={{
                        borderWidth: 3,
                        borderColor:colorMessage,
                        borderRadius: 30,
                        padding: window.height/45,
                        paddingTop: 20,
                        marginHorizontal: 40,
                        marginTop: 15,
                        marginBottom: 30,
                        height: window.height/3.6,
                        color: "white",
                        fontFamily:"Gothic",
                        fontSize: 18,
                        justifyContent:"center"
                    }}
                    onChangeText={onChangeMessage}
                    value={message}
                    multiline
                    placeholder="Message*"
                    placeholderTextColor="white"
                    returnKeyType="done"
                    blurOnSubmit={true}
                    onSubmitEditing={()=> {Keyboard.dismiss()}}
                    onEndEditing={()=> {
                        if(message.length === 0 && colorMessage === "white") onChangeColorMessage("#F9920A")
                        else if(colorMessage === "#F9920A") onChangeColorMessage("white")
                    }}
                />
                <TouchableOpacity
                    style={{ height: 50, width: 50, alignSelf:"center", borderRadius: 60 }}
                    onPress={() => {
                        if(name.length === 0) Alert.alert("Vous devez obligatoirement entrer votre nom.")
                        else if(mail.length === 0) Alert.alert("Vous devez obligatoirement entrer votre adresse mail.")
                        else if(!mail.match(/^[a-z0-9\_.-]+@[a-z0-9]+\.[a-z]{2,6}$/)) Alert.alert("L'adresse mail entrée n'est pas dans le bon format.")
                        else if(message.length === 0) Alert.alert("Vous devez obligatoirement entrer un message.")
                        else {
                            onChangeName("")
                            onChangeMail("")
                            onChangePhone("")
                            onChangeMessage("")
                            Vibration.vibrate([100])
                            Alert.alert(`Nom : ${name}\nAdresse mail : ${mail}${phone.length > 0 ? `\nTéléphone : ${phone}` : ""}\nMessage : ${message}`)
                        }
                    }}
                >
                    <Ionicons
                        name={`${Platform.OS === "ios" ? "ios" : "md"}-checkmark-circle`}
                        color="white"
                        size={50}
                    />
                </TouchableOpacity>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

export default ContactScreen