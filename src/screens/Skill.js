import React from 'react'
import { Text, SafeAreaView, View, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import { useFonts } from 'expo-font'

import Title from "../components/Title.js"

const achievementsdata = require("../../achievements.json")

const SkillScreen = ({ navigation, route }) => {
    const [loaded] = useFonts({
        Gothic: require('../../assets/fonts/Gothic.ttf'),
        GothicBold: require('../../assets/fonts/Gothic-Bold.ttf')
    })
    if (!loaded) {
        return null
    }
    const skill = route.params.paramKey
    const achievements = achievementsdata.filter(d => d.skill === skill.title)
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor:"#FFD45A" }}>
            <Title title={skill.title} returnArrow={true} navigation={navigation}/>
            <ScrollView>
                <Text style={{ flex: 1, fontSize: 16, fontFamily:"Gothic", color:"white", textAlign: "justify", margin: 20 }}>
                    {skill.desc}
                </Text>

                <View style={{ flex: 1, backgroundColor:"#F9920A", borderRadius: 30, marginHorizontal:30, marginVertical:40, alignItems:"center", justifyContent:"center"}}>
                    <Text style={{ flex: 1, fontSize: 16, fontFamily:"GothicBold", color:"white", textAlign: "justify", marginVertical: 15 }}>
                        Nos réalisations dans ce domaine
                    </Text>
                    {achievements.length === 0 ?
                        <Text style={{ flex: 1, fontSize: 15, fontFamily:"Gothic", color:"white", textAlign: "justify", marginBottom: 15, alignSelf:"center" }}>
                            Aucune réalisation pour le moment
                        </Text>
                    :
                        achievements.map(item => {
                            return (
                                <TouchableOpacity
                                    key={item.id}
                                    style={{ flex: 12, alignItems:"center", justifyContent:"center" }}
                                    onPress={() => navigation.navigate("Achievement", {paramKey: item})}
                                >
                                    <Text style={{ flex: 1, fontSize: 15, fontFamily:"Gothic", color:"white", textAlign: "justify", marginBottom: 15 }}>
                                        {item.title}
                                    </Text>
                                    <View style={{ height: 1, width: 200, borderWidth: 0.5, marginBottom:15, borderColor:"white" }}/>
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
                <Text style={{ flex: 2, fontSize: 15, fontFamily:"Gothic", color:"white", textAlign: "justify", margin:20 }}>
                    {skill.desc2 ? skill.desc2 : null}
                </Text>
            </ScrollView>
        </SafeAreaView>
    )
}

export default SkillScreen