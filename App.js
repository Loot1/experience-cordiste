import React from 'react'
import { Platform, SafeAreaView, Image } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Ionicons from 'react-native-vector-icons/Ionicons'

import AchievementsNavigation from "./src/navigations/Achievements.js"
import ContactScreen from "./src/screens/Contact.js"
import HomeScreen from "./src/screens/Home.js"
import SkillsNavigation from "./src/navigations/Skills.js"

const Tab = createBottomTabNavigator()

export default function App() {
    const [screen, setScreen] = React.useState(
        <SafeAreaView style={{ flex: 1, justifyContent:"center", backgroundColor: "#FFD45A", alignItems:"center" }}>
            <Image
                style={{ width: 150, height: 237 }}
                source={require('./assets/img/logo.png')}
            />
        </SafeAreaView>
    )
    setTimeout(() => setScreen(
        <NavigationContainer>
            <Tab.Navigator
                initialRouteName="Home"
                tabBarOptions={{
                    activeTintColor:"#ffffff",
                    inactiveTintColor:"#eeeeee",
                    style:{backgroundColor: "#F9920A", borderTopWidth: 0},
                    showLabel:false
                }}
            >
                <Tab.Screen
                    name="Home"
                    component={HomeScreen}
                    options={{
                        tabBarIcon: ({ color }) => (
                            <Ionicons
                                name={`${Platform.OS === "ios" ? "ios" : "md"}-home`}
                                size={30}
                                color={color}
                            />
                        )
                    }}
                />
                <Tab.Screen
                    name="Skills"
                    component={SkillsNavigation}
                    options={{
                        tabBarIcon: ({ color }) => (
                            <Ionicons
                                name={`${Platform.OS === "ios" ? "ios" : "md"}-hammer`}
                                size={30}
                                color={color}
                            />
                        )
                    }}
                />
                <Tab.Screen
                    name="Achievements"
                    component={AchievementsNavigation}
                    options={{
                        tabBarIcon: ({ color }) => (
                            <Ionicons
                                name={`${Platform.OS === "ios" ? "ios" : "md"}-images`}
                                size={30}
                                color={color}
                            />
                        )
                    }}
                />
                <Tab.Screen
                    name="Contact"
                    component={ContactScreen}
                    options={{
                        tabBarIcon: ({ color }) => (
                            <Ionicons
                                name={`${Platform.OS === "ios" ? "ios" : "md"}-call`}
                                size={30}
                                color={color}
                            />
                        )
                    }}
                />
            </Tab.Navigator>
        </NavigationContainer>
    ), 1500)
    return screen
}